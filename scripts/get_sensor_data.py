#!/usr/bin/env python3

import hid
import struct
import datetime

def write_data(data):
    now = datetime.datetime.now()

    w_str = f"{now},{data}\n"

    with open("/home/thb1-local/Documents/sensor_data.txt", "a") as file:
        file.write(w_str)

def get_sensor_data():
    h = hid.device()
    h.open(0x2fe3, 0x0006)
    h.set_nonblocking(0)

    a, = struct.unpack("<I", bytes(h.read(4)))

    h.close()

    return a

def main():
    data = get_sensor_data()
    write_data(data)

if __name__ == "__main__":
    main()
