#!/usr/bin/env bash

# https://docs.zephyrproject.org/latest/boards/arm/nrf52840dongle_nrf52840/doc/index.html

function check_error {
        if [ $? -ne 0 ]; then
                echo ''
                echo "==== ${1}"
                echo ''
                exit 1
        fi
}

pkg_name='lighty.zip'

west build -b nrf52840dongle_nrf52840
check_error 'Building fail'

nrfutil pkg generate --hw-version 52 --sd-req=0x00 --application build/zephyr/zephyr.hex --application-version 1 "${pkg_name}"
check_error 'Packaging fail'

nrfutil dfu usb-serial -pkg "${pkg_name}" -p /dev/ttyACM4
check_error 'Flashing fail'

rm "${pkg_name}"
check_error 'Package deletion failed'

exit 0
