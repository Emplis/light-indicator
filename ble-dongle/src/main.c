#include "zephyr/bluetooth/gap.h"
#include "zephyr/bluetooth/gatt.h"
#include <string.h>

#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>

#include <zephyr/kernel.h>

#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/sys/util.h>

#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/sys/byteorder.h>

#include <zephyr/logging/log.h>

#define LOG_LEVEL LOG_LEVEL_DBG
LOG_MODULE_REGISTER(ead_peripheral_sample, LOG_LEVEL);

extern void do_usb_stuff(void);
extern void send_report(uint8_t buf[], size_t size);

struct light_sensor {
        uint16_t heading;
	uint8_t light_level[3];
} __packed;

static void create_adv(struct bt_le_ext_adv **adv)
{
        int err;
        struct bt_le_adv_param params;

        memset(&params, 0, sizeof(struct bt_le_adv_param));

        // params.options |= BT_LE_ADV_OPT_EXT_ADV;

	params.id = BT_ID_DEFAULT;
        params.sid = 0;
	params.interval_min = BT_GAP_ADV_FAST_INT_MIN_2;
	params.interval_max = BT_GAP_ADV_FAST_INT_MAX_2;

	err = bt_le_ext_adv_create(&params, NULL, adv);
        if (err) {
                LOG_ERR("Failed to create advertiser (%d)", err);
        }
}

static void start_adv(struct bt_le_ext_adv *adv)
{
        int err;
        int32_t timeout = 0;
        uint8_t num_events = 0;

        struct bt_le_ext_adv_start_param start_params;

        start_params.timeout = timeout;
        start_params.num_events = num_events;

        err = bt_le_ext_adv_start(adv, &start_params);
        if (err) {
                LOG_ERR("Failed to start advertiser (%d)", err);
        }

        LOG_DBG("Advertiser started.");
}

static void set_ad_data(struct bt_le_ext_adv *adv, struct light_sensor data)
{
        int err;

	uint8_t illum_data[2+3] = {
		BT_UUID_16_ENCODE(BT_UUID_GATT_ILLUM_VAL),
		0x00,
		0x00,
		0x00,
	};

	illum_data[2] = data.light_level[0];
	illum_data[3] = data.light_level[1];
	illum_data[4] = data.light_level[2];

	struct bt_data ad[] = {
		BT_DATA_BYTES(BT_DATA_FLAGS, BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR),
		BT_DATA(BT_DATA_NAME_COMPLETE, CONFIG_BT_DEVICE_NAME, sizeof(CONFIG_BT_DEVICE_NAME) - 1),
		BT_DATA(BT_DATA_SVC_DATA16, illum_data, ARRAY_SIZE(illum_data))
	};

	err = bt_le_ext_adv_set_data(adv, ad, 3, NULL, 0);
        if (err) {
                LOG_ERR("Failed to set ad data");

                k_panic();
        }

        // LOG_HEXDUMP_DBG((uint8_t*)&data, sizeof(data), "Ad data updated");
}

static void update_data(struct bt_le_ext_adv *adv, uint32_t light_level)
{
	struct light_sensor light_data;

	light_data.heading = 0;
	sys_put_le24((uint32_t)light_level, light_data.light_level);

	set_ad_data(adv, light_data);

	send_report((uint8_t *)&light_level, 4);
}

/* Data of ADC io-channels specified in devicetree. */
static const struct adc_dt_spec adc_channels[] = {
	ADC_DT_SPEC_GET_BY_IDX(DT_PATH(zephyr_user), 0),
};

static void do_adc_stuff(struct bt_le_ext_adv *adv)
{
        int err;
	uint16_t buf;
	struct adc_sequence sequence = {
		.buffer = &buf,
		/* buffer size in bytes, not number of samples */
		.buffer_size = sizeof(buf),
	};

	/* Configure channels individually prior to sampling. */
	for (size_t i = 0U; i < ARRAY_SIZE(adc_channels); i++) {
		if (!device_is_ready(adc_channels[i].dev)) {
			printk("ADC controller device not ready\n");
			return;
		}

		err = adc_channel_setup_dt(&adc_channels[i]);
		if (err < 0) {
			printk("Could not setup channel #%d (%d)\n", i, err);
			return;
		}
	}

        while (1) {
		printk("ADC reading:\n");
		for (size_t i = 0U; i < ARRAY_SIZE(adc_channels); i++) {
			int32_t val_mv;

			printk("- %s, channel %d: ",
			       adc_channels[i].dev->name,
			       adc_channels[i].channel_id);

			(void)adc_sequence_init_dt(&adc_channels[i], &sequence);

			err = adc_read(adc_channels[i].dev, &sequence);
			if (err < 0) {
				printk("Could not read (%d)\n", err);
				continue;
			} else {
				printk("%"PRIu16, buf);
			}

			/* conversion to mV may not be supported, skip if not */
			val_mv = buf;
			err = adc_raw_to_millivolts_dt(&adc_channels[i],
						       &val_mv);
			if (err < 0) {
				printk(" (value in mV not available)\n");
			} else {
				printk(" = %"PRId32" mV\n", val_mv);

				update_data(adv, val_mv);
			}
		}

		k_sleep(K_MSEC(1000));
	}
}

void main(void)
{
        int err;
        struct bt_le_ext_adv *adv = NULL;

        err = bt_enable(NULL);
        if (err) {
                LOG_ERR("Bluetooth init failed (err %d)", err);
        }

        LOG_DBG("Bluetooth initialized.");

        create_adv(&adv);
        start_adv(adv);

        struct light_sensor data;

        data.light_level = 0;

	do_usb_stuff();

        do_adc_stuff(adv);
	
	return;
}
