# example
"""
height = 60.0
width = 80.0
thickness = 10.0
diameter = 22.0
padding = 12.0

result = (cq.Workplane("XY")
          .box(height, width, thickness)
          .faces(">Z").workplane().hole(diameter)
          .faces(">Z").workplane()
          .rect(height - padding,width - padding,forConstruction=True)
          .vertices()
          .cboreHole(2.4, 4.4, 2.1)
          .edges("|Z").fillet(4.0)
          .edges("|Y").fillet(2.0)
)
"""

rec0_z = 40.0
rec1_z_offset = 4

front_thickness = 14.0
back_thickness = 4.0

rec1_y = 30.0
rec0_y = rec1_y + front_thickness + back_thickness

rec_x = 30.0

rec0 = cq.Workplane("XY").box(rec_x, rec0_y, rec0_z)
rec1 = cq.Workplane("XY").box(rec_x, rec1_y, rec0_z - rec1_z_offset)

#rec3 = rec0.cut(rec1.translate((0, ((rec0_y - rec1_y)/2 - 4)/2, -4)))

y_translation = (rec1_y/2 - rec0_y/2) + 4
z_translation = -4/2
rec3 = rec0.cut(rec1.translate((0, -y_translation, z_translation)))

rec4_x = 18.0
rec4_y = 14.0
rec4_z = rec0_z

rec4 = (cq.Workplane("XY")
        .box(rec4_x, rec4_y, rec4_z)
        .union(cq.Workplane("XY")
               .box(10, 10, 20)
               .translate((0,0,-20))))

fillet_radius = 4
#yt2 = (((rec0_y/2) - (rec4_y/2)) - 4) / 2
yt2 = +(rec4_y/2 - rec0_y/2) + 4
rec5 = (rec3
        .edges("<Z")
        .fillet(1)
        .edges("<X[0]")
        .fillet(fillet_radius)
        .cut(rec4.translate((0, yt2, 6))))
# rec5 = rec5.edges("<Z[-1]").fillet(1.0)
# Render the solid

show_object(rec5)
cq.exporters.export(rec5,'result.step')